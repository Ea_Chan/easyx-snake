# EasyxSnake

## 介绍

衣陈的贪吃蛇

游戏文件位于Debug文件夹

20220821更新，修复头文件下的重定义，完成Unicode下的outtextxy（）（L宏与wstring aa.c_str() ）

20220811更新，实现完全基于easyx窗口的交互

## 框架/轮子/抓手——EasyX

本代码是基于EasyX贪吃蛇游戏，借助其库`#include<graphics.h>`提供的一些画图和文本输出的方法来实现游戏图形化。

Easyx环境搭建： [前往EasyX官网](https://easyx.cn/)按提示进行操作即可，并推荐粗览官方文档了解大致特性。

本项目应用到的方法：

```c++
#include<graphics.h>
initgraph();//初始化EasyX窗口对象方法 （）
setbkcolor(/*颜色*/);//设置窗口背景颜色			
settextcolor(/*颜色*/)；//设置文本颜色
setfillcolor(/*颜色*/);//设置填充颜色
cleardevice();//清空窗口并用背景颜色布满窗口
outtextxy(/* int x,int y,char[] */);//输出char[]类型或匿名字符串“”
fillrectangle(/*(左上角，坐标),(右下角，坐标)*/)//绘制填充矩形，共四个int参数
```

> 注意，outtext方法的参数变量char[]，字符型静态数组。当使用“ ”为参时，需要：

 ![image-20220801033649323](https://s2.loli.net/2022/08/11/7SAeHXuNla8DUyF.png)

**右键项目sln->属性->高级->字符集->将设置更改为“使用多字节字符集”**

## 20220731

游戏说明

wsad移动

请确认已调整至英文输入状态

按任意键以继续

（面向百度）开发日志：

是真不想写什么开发日志，很普通的算法。

把数据结构设计和面向对象做好。

第一次跳出cmd编程，只能说easyx只是个抓手吧，提供了画贪吃蛇的画笔而已，游戏的核心是什么呢？我觉得还是数据结构和算法，其他不过基于这个画（或者说渲染）一下罢了，而贪吃蛇的算法逻辑不过是比较判断，那就只剩数据结构了吧。

是的，设计Snake类，Food类，他们的成员 属性和方法就挺有意思的,模块化和面向对象的编程思想

## 202207310140

美化了一下代码，最大程度做好了封装，将面向对象思想与面向过程思想较好融合（面向过程体现为对象活动环境的搭建与对象间的交互）。

对啊，优雅的代码本身就是最好的注释

已开源

## 202208010128

### 计分板模板

```c++
void scoredraw(int& score) {
	settextcolor(BLACK);
	settextstyle(20, 0,"楷体");
	setbkmode(TRANSPARENT);
    outtextxy(700, 0, "Score: ");
	string str= to_string(score);
	char arr[MAX];
	for(int i=0;i<=str.size(); i++)
	{
		arr[i] = str[i];
	}
	arr[str.size()+1] = '\0';
	outtextxy(765, 0,arr);
//	if(score)cout <<score<<" " << arr << endl;
}
```

> EasyX的textset方法

> 基于outtext方法只接受静态字符数组char[]这一事实

需实现int到char[]的转化，方法很多，比如

* `string容器`的`to_string`方法
* `stringstream ss`temp数据 `ss<<int;ss>>str;`

> 注意辨析string与char[]的区别

string 是<char\>类型的容器，动态，是封装char*属性和若干方法的类

char[]是静态char数组，逐char构造时需要手写'\0'

### kbhit问题

一个控制结构层次中应只有一个获取键盘事件方法，以避免竞争键位读取，因此，暂停，换速功能的接口只能封装在Snake的move()方法中。

### 面向对象代码思路：

将写在main的方法封装在mana类中。

将这一层游戏的进程这一面向过程封装为了面向对象

最终的程序逻辑为：在main中，Snake，Food,Mana的实例对象snake，food，mana的交互。

跨展性与可维护性max

## 202200811

## 将游戏完全置于easyx窗口交互

### 缓冲绘图方法

~~~C++
BeginBatchDraw();//开始缓冲绘图，渲染图像压入缓冲队列暂不显示
FlushBatchDraw();//释放缓冲绘图
EndBatchDraw();//结束缓冲绘图，回归即时渲染模式
~~~

### EasyX的GoodGame交互窗口

~~~c++
//GoodGame交互窗口，重开/退出，包含蛇的重构，窗口的重置
void Mana:: gg_then(Snake& my, Food& food, int& score) {
	//结束游戏部分的缓冲绘图，开始即时交互
	EndBatchDraw();
    
    //黑色交互窗体
	setbkcolor(BLACK);
	cleardevice();
    
    //文本背景框，字样，文本
	setbkcolor(RGB(240, 240, 240));
	setbkmode(OPAQUE);
    
	settextcolor(RED);
    
	outtextxy(150, 250, "You are dead,please input \"space\"to play once again\0");
	outtextxy(280, 280, "OR others to end\0");
    
    //执行交互
	char ch = _getch();
	switch (ch)
	{
	case 32:
		 //重构蛇，新食物坐标，计分板，恢复缓冲绘图
		 my.resetSnake();
		score = 0;
		food.newFood(my);
		setbkcolor(RGB(240, 240, 240));
		BeginBatchDraw();
		break;
	default:
		exit(0);
		break;
	}
}
~~~

### Snake的重构方法

> Topic是如何重置一个维护了堆区指针的容器，
>
> 可惜对对象显示调用析构函数。但此处此后构造的对象出不了作用域，所以我们不该切换对象，而是应该给对象做还原出厂设置

~~~c++
//delete then init
void Snake::resetSnake() {
    //使用与析构相同的方法释放容器维护指针所指向的数据
	/*~snake()；*/ 
    
    //resize方法重置容器
	this->snake.resize(0, NULL);
    
    //使用与构造相同的方法重构新蛇
    /*Snake();*/
    
}
~~~
