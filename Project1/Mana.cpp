#include"Mana.h"
#include"snake.h"
#include"Food.h"
//初次开始游戏，游戏说明交互与easyx窗口初始化
void Mana:: init_easyx() {
	srand((unsigned int)time(NULL));
	initgraph(800, 600);
	setbkcolor(RGB(240, 240, 240));
	settextcolor(RED);
	wstring a = L"WSAD to move.Space to pause.I/O to speed up/down\0";
	outtextxy(240, 250,a.c_str());
	outtextxy(200, 280,L"Please ensure your English-input-mode,then hit any to continue \0");
	char ch = _getch();
	BeginBatchDraw();

}
//背景画笔，可扩充
void Mana:: screendraw() {
	cleardevice();
	
}
//判断是否吃下食物，蛇生长一格，刷新食物（不渲染）
void Mana:: if_eat_then(Snake& my, Food& food, int& score) {
	if (my.snake.front()->x == food.x && my.snake.front()->y == food.y) {
		score++;
		Snake::node* pos = new Snake::node;
		pos->direct = my.snake.back()->direct;
		switch (pos->direct) {
		case 0:
			pos->y = my.snake.back()->y + 1;
			pos->x = my.snake.back()->x;
			break;
		case 1:
			pos->y = my.snake.back()->y - 1;
			pos->x = my.snake.back()->x;
			break;
		case 2:
			pos->y = my.snake.back()->y;
			pos->x = my.snake.back()->x + 1;
			break;
		case 3:
			pos->y = my.snake.back()->y;
			pos->x = my.snake.back()->x - 1;
			break;
		default:
			break;
		}
		my.snake.push_back(pos);
		food.newFood(my);

	}
}
//计分板模块
void Mana:: scoredraw(int& score) {
	settextcolor(BLACK);
	settextstyle(20, 0, L"楷体");
	setbkmode(TRANSPARENT);
	wstring str = to_wstring(score);
	outtextxy(700, 0,L"Score: ");
	outtextxy(765, 0,str.c_str());
	//	if(score)cout <<score<<" " << arr << endl;

}
//GoodGame交互窗口，重开/退出，包含蛇的重构，窗口的重置
void Mana::gg_then(Snake& my, Food& food, int& score) {
	//结束游戏部分的缓冲绘图，开始即时交互
	EndBatchDraw();

	//黑色交互窗体
	setbkcolor(BLACK);
	cleardevice();

	//文本背景框，字样，文本
	setbkcolor(RGB(240, 240, 240));
	setbkmode(OPAQUE);

	settextcolor(RED);

	outtextxy(150, 250, L"You are dead,please input \"space\"to play once again\0");
	outtextxy(280, 280, L"OR others to end\0");

	//执行交互
	char ch = _getch();
	switch (ch)
	{
	//重构蛇，新食物坐标，计分板，恢复缓冲绘图
	case 32:	
		my.resetSnake();
		score = 0;
		food.newFood(my);
		setbkcolor(RGB(240, 240, 240));
		BeginBatchDraw();
		break;
	//结束程序
	default:
		exit(0);
		break;
	}
}