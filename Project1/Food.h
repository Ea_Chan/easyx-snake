#pragma once
//food的newfood方法需要与snake交互
using namespace std;
class Snake;
//注意，此处食物类为了创建游戏中的单个食物实例，而是为了管理“food”这个游戏元素。
//而这个游戏元素在游戏中永远成单出现，我们可以用这个类的实例对象的成员属性来表示当前food的属性，用newfood的成员方法通过更新对象的属性来更新food。
class Food {
public:
	int x, y;
	void newFood(Snake& my);
	void fooddraw();
};