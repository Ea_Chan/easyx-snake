#include"Food.h"
#include"snake.h"
//食物生成器：遍历蛇身坐标判断食物坐标是否刷新在蛇身里
void Food::newFood(Snake& snake) {
	while (true) {
		this->x = rand() % 79;
		this->y = rand() % 59;
		int ans = 0;
		for (auto it = snake.snake.begin(); it != snake.snake.end(); it++) {
			Snake::node* pos = *it;
			if (this->x == pos->x && this->y == pos->y) {
				ans++;
				break;
			}
		}
		if (ans == 0)break;
	}


}
//食物画笔
void Food::fooddraw() {
	setfillcolor(BLACK);
	fillrectangle(this->x * 10, this->y * 10, (this->x + 1) * 10, (this->y + 1) * 10);
}