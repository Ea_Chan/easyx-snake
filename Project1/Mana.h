#pragma once
//mana对象的交互对象是food和snake（以if_eat_then为代表）
using namespace std;
class Snake;
class Food;
class Mana {
public:
	void init_easyx();
	void screendraw();
	void if_eat_then(Snake& my, Food& food, int& score);
	void scoredraw(int& score);
	void gg_then(Snake& my, Food& food, int& score);
};