#include"snake.h"
//随机初始化一条长度3的蛇
Snake::Snake() {
	//*new风格先创建对象指针，再写入有址匿名对象的成员属性——生成随机坐标与方向的蛇头
	Snake::node* _head = new node;
	_head->x = rand() % 59 + 10;//10-69
	_head->y = rand() % 39 + 10;//10-49
	_head->direct = rand() % 4;
	this->snake.push_back(_head);
	//蛇成长逻辑：沿前一个元素的direct的反向延长一格，尾插实现
	for (int i = 0; i < 2; i++) {
		Snake::node* pos = new node;
		pos->direct = this->snake.back()->direct;
		switch (pos->direct) {
		case up:
			pos->y = this->snake.back()->y + 1;
			pos->x = this->snake.back()->x;
			break;
		case down:
			pos->y = this->snake.back()->y - 1;
			pos->x = this->snake.back()->x;
			break;
		case left:
			pos->y = this->snake.back()->y;
			pos->x = this->snake.back()->x + 1;
			break;
		case right:
			pos->y = this->snake.back()->y;
			pos->x = this->snake.back()->x - 1;
			break;
		default:
			break;
		}
		this->snake.push_back(pos);

	}
}
//坐标检查（开发时调试用）
void Snake::check()
{
	for (auto it = this->snake.begin(); it != this->snake.end(); it++) {
		node* pos = *it;
		fillrectangle(pos->x * 10, pos->y * 10, (pos->x + 1) * 10, (pos->y + 1) * 10);
		cout << pos->x << "," << pos->y << " ";
	}
	cout << "over\n";
}
//蛇移动逻辑：获取键反确认方向，所有节点沿自身方向前移一格，新蛇头=旧蛇头矢向前移，中间蛇身无变化，蛇尾被删去。实现：计算新蛇头数据，对容器头插尾删实现逻辑。
void Snake::move(int& speed) {
	
	Snake::node* newhead = new node;

	//以下为新蛇头方向算法
	//对于vs2022，kbhit()与getch()过期不可用，用这两个组合
	//全局应只存在一次读取键盘事件，防止竞争冲突。(读入缓冲)
	if (_kbhit(/*判断是否存在键返函数*/)) {
		char ch = _getch(/*读取键反函数*/);
		wstring arr = L"Game_Pause,hit any key to continue.";
		int wide = textwidth(arr.c_str());
		switch (ch)
		{
		case 'W':
		case 'w':
			if (this->snake.front()->direct == 2 || this->snake.front()->direct == 3)
				newhead->direct = 0;
			else newhead->direct = this->snake.front()->direct;
			break;
		case 'S':
		case 's':
			if (this->snake.front()->direct == 2 || this->snake.front()->direct == 3)
				newhead->direct = 1;
			else newhead->direct = this->snake.front()->direct;
			break;
		case 'A':
		case 'a':
			if (this->snake.front()->direct == 1 || this->snake.front()->direct == 0)
				newhead->direct = 2;
			else newhead->direct = this->snake.front()->direct;
			break;
		case 'D':
		case 'd':
			if (this->snake.front()->direct == 1 || this->snake.front()->direct == 0)
				newhead->direct = 3;
			else newhead->direct = this->snake.front()->direct;
			break;
		case 32:
			newhead->direct = this->snake.front()->direct;
		//	cout << "pause\n";
			settextcolor(BLACK);
			setbkmode(TRANSPARENT);
			settextstyle(20, 0, L"楷体");
			outtextxy((800 - wide) / 2, 200, arr.c_str());
			FlushBatchDraw();
			ch=_getch();
			break;
		case 'i':
		case 'I':
			if (speed >= 20) speed -= 10;
			newhead->direct = this->snake.front()->direct;
			break;
		case 'O':
		case'o':
			speed += 10;
			newhead->direct = this->snake.front()->direct;
			break;
		default:
			newhead->direct = this->snake.front()->direct;
			break;

		}
	
	}
	else newhead->direct = this->snake.front()->direct;
//	cout << newhead->direct<<" ";

	//以下为新蛇头坐标算法
	switch (newhead->direct) {
	case up:
		newhead->y = this->snake.front()->y-1 ;
		newhead->x = this->snake.front()->x;
		break;
	case down:
		newhead->y = this->snake.front()->y+1 ;
		newhead->x = this->snake.front()->x;
		break;
	case left:
		newhead->y = this->snake.front()->y;
		newhead->x = this->snake.front()->x-1 ;
		break;
	case right:
		newhead->y = this->snake.front()->y;
		newhead->x = this->snake.front()->x+1 ;
		break;
	default:
		break;
	}
//	cout << this->snake.front()->x << this->snake.front()->y << endl;
	
	//以下为蛇对象更新算法：头插-delete-尾删
	this->snake.push_front(newhead);
//	cout << this->snake.front()->x << this->snake.front()->y << endl;
	//不再被容器维护的节点，应delete回收
	if (this->snake.back() != NULL) {
		delete this->snake.back();
		this->snake.back() = NULL;
	}
	this->snake.pop_back();

}
//蛇画笔，遍历容器打印坐标
void Snake::snakedraw() {
//	Sleep(SPEED);
	for (auto it = this->snake.begin(); it != this->snake.end(); it++) {
		node* pos = *it;
		if (it == this->snake.begin())
			setfillcolor(BLUE);	
		else 
			setfillcolor(BLACK);
		fillrectangle(pos->x * 10, pos->y * 10, (pos->x + 1) * 10, (pos->y + 1) * 10);
	}
}
//死亡判定：撞墙，撞身（遍历蛇身是否有与蛇头坐标相同）
bool Snake::if_gg() {
	
		//撞墙
		if (this->snake.front()->x < 0/* && my.snake.front()->direct == 2*/) return true;
		if (this->snake.front()->y < 0 /*&& my.snake.front()->direct == 0*/) return true;
		if (this->snake.front()->x > 79 /*&& my.snake.front()->direct == 3*/) return true;
		if (this->snake.front()->y > 59 /*&& my.snake.front()->direct == 1*/) return true;
		//撞身
		int ans = 0;
		Snake::node* head = this->snake.front();
		for (auto it = this->snake.begin() + 1; it != this->snake.end(); it++) {
			Snake::node* body = *it;
			if (head->x == body->x && head->y == body->y)
			{
				ans++;
				break;
			}

		}
		if (ans) return true;
		else return false;
	
}
//析构释放蛇容器
Snake::~Snake() {
	for (auto it = this->snake.begin(); it != this->snake.end(); it++) {
		node* pos = *it;
		if (pos != NULL)
		{
			delete pos;
			pos = NULL;
		}
	}
	//for (auto it = this->snake.begin(); !this->snake.empty(); this->snake.pop_front()){}
}
//delete then init
void Snake::resetSnake() {
	for (auto it = this->snake.begin(); it != this->snake.end(); it++) {
		node* pos = *it;
		if (pos != NULL)
		{
			delete pos;
			pos = NULL;
		}
	}
	this->snake.resize(0, NULL);
	//*new风格先创建对象指针，再写入有址匿名对象的成员属性——生成随机坐标与方向的蛇头
	Snake::node* _head = new node;
	_head->x = rand() % 59 + 10;//10-69
	_head->y = rand() % 39 + 10;//10-49
	_head->direct = rand() % 4;
	this->snake.push_back(_head);
	//蛇成长逻辑：沿前一个元素的direct的反向延长一格，尾插实现
	for (int i = 0; i < 2; i++) {
		Snake::node* pos = new node;
		pos->direct = this->snake.back()->direct;
		switch (pos->direct) {
		case up:
			pos->y = this->snake.back()->y + 1;
			pos->x = this->snake.back()->x;
			break;
		case down:
			pos->y = this->snake.back()->y - 1;
			pos->x = this->snake.back()->x;
			break;
		case left:
			pos->y = this->snake.back()->y;
			pos->x = this->snake.back()->x + 1;
			break;
		case right:
			pos->y = this->snake.back()->y;
			pos->x = this->snake.back()->x - 1;
			break;
		default:
			break;
		}
		this->snake.push_back(pos);

	}
}
