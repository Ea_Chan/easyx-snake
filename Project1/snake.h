#pragma once
#include<iostream>
#include<windows.h>
#include<graphics.h>
#include<conio.h>
#include<deque>
#include<string>
#define SQU 10
#define MAX 16
using namespace std;
//面向对象：snake与food
//蛇的管理类
class Snake {
public:
	//蛇的单元：记录自身方向的矢量小方格（主要目的是感知head与tail的行进方向，中间蛇身记录的方向仅起到传导作用）
	class node {
	public:
		int x, y;
		int direct;//0up 1dowm 2left 3right，便于生成初始随机方向
	};
	enum DIR{up,down,left,right};
	//使用维护指针的风格来定义蛇容器，减少临时node尾插到容器时的拷贝构造的空间损耗。
	//使用容器的风格，通过尾插，头插尾删的方法实现蛇实例对象的创建，增长，移动
	//蛇的实例对象：节点类的指针动态数组
	deque<node*> snake;

	//方法
	Snake();
	~Snake();
	void resetSnake();
	void snakedraw();
	void move(char& ch);
	void move(int& speed);
	void check();
	bool if_gg();;
};

