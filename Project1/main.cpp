#pragma once
#include"snake.h"
#include"Food.h"
#include"Mana.h"
#define SQU 10
#define MAX 16
using namespace std;
//面向过程：游戏流程，对象snake与food的交互，而我们可以把面向过程封装为一层管理类对象，
//面向对象编程： Snake，Food，Mana在main中的实例对象my，food，mana的交互
//我觉得也没必要封装到这个程度，管理对象game有点抽象
int main() {
//	FLAG:
	//main函数声明的变量作用于游戏全局
	Mana game;
	Snake snake;
	Food food;
	food.newFood(snake);
	int num = 0;
	int score = 0;
	int speed = 100;
	game.init_easyx();
	while (true)
	{	//游戏流程如函数名描述
		//temp.Gameset(speed);
		//begin-flush缓冲绘图方法，防止闪图。
		game.screendraw();
		game.scoredraw(score);
		snake.snakedraw();
		food.fooddraw();
		FlushBatchDraw();
		Sleep(speed);
		snake.move(speed);
		//封装方法同时把蛇和食物两个对象传进去即可。
		game.if_eat_then(snake, food, score);
		if (snake.if_gg()) game.gg_then(snake, food, score);
	}
	return 0;
}